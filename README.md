# LFortran Python wrappers

Python wrappers to the C++ code / library
[LFortran](https://gitlab.com/lfortran/lfortran),
a modern open-source (BSD licensed) interactive Fortran compiler built on top of
LLVM.

Currently this repository contains both C++ sources and Python sources. As the
main [C++ repository](https://gitlab.com/lfortran/lfortran) matures, we will
simply depend on it and remove the C++ sources from this repository.
